import java.awt.*;
import java.awt.event.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import javax.swing.*;


public class Klient extends JApplet {

	static final long serialVersionUID = 1L;
	int APPLET_WIDTH = 300, APPLET_HEIGHT=100;
    int glosyDlaJacek;
    int glosyDlaPlacek;
    JLabel labelJacek;
    JLabel labelPlacek;
    JButton Placek;
    JButton Jacek;
    
    // ------------------------------------------------------------
    //  Ustawia GUI
    // ------------------------------------------------------------
    public void init ()
    {
	glosyDlaJacek = 0;
	
	Jacek = new JButton ("Glosuj na Jacka!");
	Jacek.addActionListener (new JacekButtonListener());
	
	Placek = new JButton ("Glosuj na Placka!");
	Placek.addActionListener (new PlacekButtonListener());
	
	labelJacek = new JLabel ("Glosy dla Jacka: " + Integer.toString (glosyDlaJacek));
	labelPlacek = new JLabel ("Glosy dla Placka: " + Integer.toString (glosyDlaPlacek));
	
	Container cp = getContentPane();
	cp.setBackground (Color.cyan);
	cp.setLayout (new FlowLayout());
	
	cp.add (Jacek);
	cp.add (labelJacek);
	
	cp.add (Placek);
	cp.add (labelPlacek);

	setSize (APPLET_WIDTH, APPLET_HEIGHT);
    }


    // *******************************************************************
    //  Reprezentuje listener dla akcji wcisniecia przycisku
    // *******************************************************************

    public class PlacekButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)
        {
        	
        	try{
    			Socket s=new Socket("127.0.0.1",1201);
    			DataInputStream din = new DataInputStream(s.getInputStream());
    			DataOutputStream dout = new DataOutputStream(s.getOutputStream());
    			String msgin="",msgout="";
    			
    			//	while(true)
    			{
    			msgout=Integer.toString (glosyDlaPlacek);
				dout.writeUTF(msgout);
				msgin=din.readUTF();
				System.out.print(msgin);
				glosyDlaPlacek=Integer.valueOf(msgin);
				labelPlacek.setText ("Glosy dla Placka: " + Integer.toString (glosyDlaPlacek));
                repaint ();
                s.close();
    			}
                
    			}catch (Exception e)
    			{
    				System.out.print("wyjatek");
    				
    				
    			}
          
            
        }
    
} 
    
    public class JacekButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)
        {
        	
        	try{
    			Socket s=new Socket("127.0.0.1",1201);
    			DataInputStream din = new DataInputStream(s.getInputStream());
    			DataOutputStream dout = new DataOutputStream(s.getOutputStream());
    			String msgin="",msgout="";
    			
    		//	while(true)
    			{
    			//zapytanie, a serwer wykonuje zadanie
    			msgout=Integer.toString (glosyDlaJacek);
				dout.writeUTF(msgout);
				msgin=din.readUTF();
				System.out.print(msgin);
				glosyDlaJacek=Integer.valueOf(msgin);
				labelJacek.setText ("Glosy dla Jacka: " + Integer.toString (glosyDlaJacek));
				s.close();
                repaint ();
    			}
                
    			}catch (Exception e)
    			{
    				System.out.print("wyjatek");
    				
    			}
          
            
        }
    
}
}